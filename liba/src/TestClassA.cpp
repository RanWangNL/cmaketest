//
// Created by user on 8-6-16.
//

#include "TestClassA.h"
#include <bits/stdc++.h>
TestClassA::TestClassA() {
    std::cout << "I am alive" << std::endl;
}

TestClassA::~TestClassA() {
    std::cout << "I am dead" << std::endl;
}

void TestClassA::printsomething() {
    std::cout << "I am just trying to do something fun" << std::endl;
}
