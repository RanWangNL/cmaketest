#include <iostream>
#include <memory>
#include "liba/src/TestClassA.h"

using namespace std;

int main() {
    auto myclass = make_shared<TestClassA>();
    cout << "Hello, World!" << endl;
    return 0;
}